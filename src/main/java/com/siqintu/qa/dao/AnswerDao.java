package com.siqintu.qa.dao;

import com.siqintu.qa.entity.Answer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnswerDao extends CrudRepository<Answer, Long> {
    List<Answer> findAnswerByQuestion_Id(Long id);
}
