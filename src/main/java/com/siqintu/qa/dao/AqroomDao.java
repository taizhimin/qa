package com.siqintu.qa.dao;

import com.siqintu.qa.entity.Aqroom;
import org.springframework.data.repository.CrudRepository;

public interface AqroomDao extends CrudRepository<Aqroom, Long> {
}
