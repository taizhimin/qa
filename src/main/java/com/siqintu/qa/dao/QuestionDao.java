package com.siqintu.qa.dao;

import com.siqintu.qa.entity.Question;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuestionDao extends CrudRepository<Question, Long> {
    List<Question> findQuestionByAqroom_Id(Long id);
}
