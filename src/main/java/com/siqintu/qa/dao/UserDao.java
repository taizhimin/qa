package com.siqintu.qa.dao;

import com.siqintu.qa.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, Long> {
    User findUserByUsername(String usernmae);
}
