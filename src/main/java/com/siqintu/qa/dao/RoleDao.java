package com.siqintu.qa.dao;

import com.siqintu.qa.entity.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleDao extends CrudRepository<Role, Long> {
}
