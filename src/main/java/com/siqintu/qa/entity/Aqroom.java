package com.siqintu.qa.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 答疑室
 */
@Entity
@Table(name = "aqroom")
public class Aqroom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;//答疑室编号
    private String name;//答疑室名称
    @OneToOne
    @JoinColumn(name = "create_by", referencedColumnName = "id")
    private User createBy;//答疑室创建人
    @DateTimeFormat(pattern = "YYYY-MM-dd HH:mm:ss")
    private Date createDate;//创建时间
   /* @OneToMany
    @JoinColumn(name = "aqroom_id",referencedColumnName = "id")
    private List<Question> questions;*/

    public Aqroom() {

    }

    public Aqroom(long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

/*    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }*/
}
