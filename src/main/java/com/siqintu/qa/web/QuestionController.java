package com.siqintu.qa.web;

import com.siqintu.qa.dao.AnswerDao;
import com.siqintu.qa.dao.AqroomDao;
import com.siqintu.qa.dao.QuestionDao;
import com.siqintu.qa.entity.Answer;
import com.siqintu.qa.entity.Aqroom;
import com.siqintu.qa.entity.Question;
import com.siqintu.qa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
public class QuestionController {
    @Autowired
    private QuestionDao questionDao;
    @Autowired
    private AqroomDao aqRoomDao;
    @Autowired
    private AnswerDao answerDao;

    @RequestMapping("/aqroom")
    public String index(long id, Model model) {
        model.addAttribute("aqroom", aqRoomDao.findById(id).get());
        model.addAttribute("list", questionDao.findQuestionByAqroom_Id(id));
        return "aq";
    }

    @RequestMapping("/question")
    public String question(long id, Model model) {
        model.addAttribute("question", questionDao.findById(id).get());
        model.addAttribute("list", answerDao.findAnswerByQuestion_Id(id));
        return "q";
    }

    @GetMapping("/question/form")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String questionForm(long id, Model model) {
        model.addAttribute("question", questionDao.findById(id).get());
        return "questionForm";
    }

    @GetMapping("/question/add")
    public String questionAdd(long id, Model model) {
        Question question = new Question();
        question.setAqroom(new Aqroom(id));
        model.addAttribute("question", question);
        return "questionForm";
    }

    @PostMapping("/question/save")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String question(Question question, Model model) {
        if (question.getId() == null) {
            User user = (User) SecurityContextHolder.getContext()
                    .getAuthentication()
                    .getPrincipal();
            question.setCreateDate(new Date());
            question.setCreateBy(user);
        } else {
            Question question1 = questionDao.findById(question.getId()).get();
            question.setCreateBy(question1.getCreateBy());
            question.setAqroom(question1.getAqroom());
            question.setCreateDate(question1.getCreateDate());
            question.setAnswers(question1.getAnswers());
        }
        questionDao.save(question);
        return "redirect:/aqroom?id=" + question.getAqroom().getId();
    }

    @GetMapping("/question/delete")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String delete(Question question, Model model) {
        String url = "redirect:/aqroom?id=" + questionDao.findById(question.getId()).get().getAqroom().getId();
        questionDao.delete(question);
        return url;
    }

    @PostMapping("/answer")
    public String answer(Answer answer, Model model) {
        if (answer.getId() == null) {
            User user = (User) SecurityContextHolder.getContext()
                    .getAuthentication()
                    .getPrincipal();
            answer.setCreateBy(user);
            answer.setCreateDate(new Date());
        } else {
            Answer answer1 = answerDao.findById(answer.getId()).get();
            answer.setCreateDate(answer1.getCreateDate());
            answer.setCreateBy(answer1.getCreateBy());
            answer.setQuestion(answer1.getQuestion());
        }
        answerDao.save(answer);
        return "redirect:/question?id=" + answer.getQuestion().getId();
    }

    @GetMapping("/answer/delete")
    @PreAuthorize(value = "hasAnyAuthority('admin')")
    public String answerDelete(Answer answer, Model model) {
        String url = "redirect:/question?id=" + answerDao.findById(answer.getId()).get().getQuestion().getId();
        answerDao.delete(answer);
        return url;
    }
}
