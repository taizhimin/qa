package com.siqintu.qa.web;

import com.siqintu.qa.dao.RoleDao;
import com.siqintu.qa.dao.UserDao;
import com.siqintu.qa.entity.Role;
import com.siqintu.qa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @PostMapping("/register")
    public String register(String username, String password, String name) {
        User user = new User();
        user.setUsername(username);
        user.setName(name);
        user.setPassword(new BCryptPasswordEncoder(4).encode(password));
        user.setRole(new Role(3));
        userDao.save(user);
        return "redirect:/login";
    }

    @GetMapping("/register")
    public String register() {
        return "register";
    }

    @GetMapping("/admin")
    @PreAuthorize(value = "hasAuthority('admin')")
    public String list(Model model) {
        model.addAttribute("list", userDao.findAll());
        return "userList";
    }

    @GetMapping("/admin/user/delete")
    @PreAuthorize(value = "hasAuthority('admin')")
    public String list(Model model, long id) {
        userDao.deleteById(id);
        return "redirect:/admin";
    }

    @PostMapping("/admin/user/save")
    @PreAuthorize(value = "hasAuthority('admin')")
    public String add(Model model, User user) {
        user.setPassword(new BCryptPasswordEncoder(4).encode(user.getPassword()));
        userDao.save(user);
        return "redirect:/admin";
    }

    @GetMapping("/admin/user/form")
    @PreAuthorize(value = "hasAuthority('admin')")
    public String form(Model model, User user) {
        if (user != null && user.getId() != null) {
            model.addAttribute("user", userDao.findById(user.getId()).get());
        }
        model.addAttribute("roleList", roleDao.findAll());
        return "userForm";
    }

    /**
     * 修改密码
     *
     * @param model
     * @return
     */
    @GetMapping("/modifypwd")
    public String toModifypwd(Model model) {
        User user = (User) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        model.addAttribute("user", user);
        return "modifypwd";
    }

    /**
     * 修改密码
     *
     * @param model
     * @param newPwd,oldPwd
     * @return
     */
    @PostMapping("/modifypwd")
    public String modifypwd(Model model, String newPwd) {
        User user = (User) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        user.setPassword(new BCryptPasswordEncoder(4).encode(newPwd));
        userDao.save(user);
        model.addAttribute("user", user);
        return "info";
    }

    /**
     * 个人信息
     *
     * @param model
     * @return
     */
    @GetMapping("/info")
    public String modifypwd(Model model) {
        User user = (User) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        model.addAttribute("user", user);
        return "info";
    }
}
