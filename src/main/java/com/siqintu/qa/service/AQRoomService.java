package com.siqintu.qa.service;

import com.siqintu.qa.dao.AqroomDao;
import com.siqintu.qa.entity.Aqroom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AQRoomService {
    @Autowired
    private AqroomDao dao;

    public Iterable<Aqroom> findList() {
        return dao.findAll();
    }
}
